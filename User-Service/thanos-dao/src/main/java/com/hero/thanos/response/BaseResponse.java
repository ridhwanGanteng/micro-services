package com.hero.thanos.response;

public class BaseResponse {

	private String responseCode;
	private String responseDesc;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	@Override
	public String toString() {
		return "BaseResponse [responseCode=" + responseCode + ", responseDesc=" + responseDesc + "]";
	}

}
