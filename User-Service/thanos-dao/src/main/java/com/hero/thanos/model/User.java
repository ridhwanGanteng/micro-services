package com.hero.thanos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", nullable = false, unique = true, updatable = false)
	private Long user_id;

	@Column(name = "nama", nullable = false, length = 64)
	private String nama;

	@Column(name = "password", nullable = false, length = 64)
	private String password;

	@Column(name = "role", nullable = false, length = 64)
	private String role;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(Long user_id, String nama, String password, String role) {
		super();
		this.user_id = user_id;
		this.nama = nama;
		this.password = password;
		this.role = role;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
