package com.hero.thanos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hero.thanos.model.User;



public interface UserDao extends CrudRepository<User, Long> {

	public abstract User findByNama(String nama);
	
	public abstract User findByNamaAndPassword(String nama, String pass);
	
	public abstract List<User> findByNamaLike(String nama);
	
	

}
