package com.hero.thanos.util;

import java.util.ArrayList;
import java.util.List;

public class CollectionHelper {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List iterToList(Iterable iterable) {
		List listObj = new ArrayList();
		for(Object obj : iterable) {
			listObj.add(obj);
		}
		return listObj;
	}
	
}
