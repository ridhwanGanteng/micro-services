package com.hero.thanos.service;

import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hero.thanos.dao.UserDao;
import com.hero.thanos.model.User;
import com.hero.thanos.util.CollectionHelper;


@Service
public class UserService {

	@Autowired
	private UserDao repository;

	public User findById(Long id) {
		return repository.findById(id).get();
	}

	public List<User> findByNama(String nama) {
		return repository.findByNamaLike(nama);
	}

	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		return CollectionHelper.iterToList(repository.findAll());
	}

	public User save(User nasabah) {
		return repository.save(nasabah);
	}

	@Transactional
	public Boolean updateUser(User u) {
		try {
			User user = repository.findById(u.getUser_id()).get();
			user.setNama(u.getNama());
			user.setPassword(u.getPassword());
			user.setRole(u.getRole());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	@Transactional
	public Boolean deleteUser(Long idUser) {
		try {
			User user = repository.findById(idUser).get();
			repository.delete(user);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public User Login(String nama, String pass) {
		return repository.findByNamaAndPassword(nama, pass);
	}

}
