package com.hero.thanos.controller;


import java.net.NetworkInterface;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hero.thanos.model.User;
import com.hero.thanos.pojo.request.ReqLogin;
import com.hero.thanos.request.UserRequest;
import com.hero.thanos.response.BaseResponse;
import com.hero.thanos.service.UserService;

import ch.qos.logback.core.net.LoginAuthenticator;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;




@RestController
@RequestMapping(value="api/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {
	
	@Autowired
	UserService service;
	
	private Log log = LogFactory.getLog(this.getClass());
	
	
	
		@RequestMapping(value="/login",method={RequestMethod.POST})
		public String Login(@RequestBody ReqLogin obj ){
			
			log.info("##USER Nama = "+obj.getNama());
			log.info("##USER Password = "+obj.getPassword());
			User u=service.Login(obj.getNama(),obj.getPassword());
			log.info("User "+u);
			if(u!=null && u.getUser_id()!=0){
				return "00";
			}
			return "01";
		}
		
		@RequestMapping(value="/test",method=RequestMethod.GET)
		public BaseResponse Test(){
			BaseResponse respon = new BaseResponse();
			respon.setResponseCode("00");
			respon.setResponseDesc("Oke connect. dari layer ke2");
			return respon;
		}
		
		@RequestMapping(value="save",method={RequestMethod.POST})
		public HashMap<String, Object> saveUser(@RequestBody ReqLogin u){
			User user = new User();
			user.setNama(u.getNama());
			user.setPassword(u.getPassword());
			user.setRole(u.getRole());
			User result=service.save(user);
			HashMap<String, Object> respon = new  HashMap<String,Object>();
		    respon.put("success", Boolean.FALSE);
			if(result!=null && result.getUser_id()!=0){
				respon.put("success", Boolean.TRUE);
			}
			return respon;
		}
		
		
		@RequestMapping(value="update",method={RequestMethod.PUT})
		public HashMap<String, Object> updateUser(@RequestBody User u){
			HashMap<String, Object> respon = new  HashMap<String,Object>();
		    respon.put("success", Boolean.FALSE);
			if(service.updateUser(u)){
				respon.put("success", Boolean.TRUE);
			}
			
			return respon;
		}
		
		
		@RequestMapping(value="delete",method={RequestMethod.DELETE})
		public HashMap<String, Object> deleteUser(@PathVariable("idUser") Long u){
			HashMap<String, Object> respon = new  HashMap<String,Object>();
		    respon.put("success", Boolean.FALSE);
			if(service.deleteUser(u)){
				respon.put("success", Boolean.TRUE);
			}
			
			return respon;
		}
		
		
		@RequestMapping(value="getBy",method={RequestMethod.GET})
		public HashMap<String, Object>  getUsetBy(@PathVariable("nama") String nama){
			HashMap<String, Object> respon = new  HashMap<String,Object>();
			respon.put("Response", service.findByNama(nama));
			respon.put("success", Boolean.TRUE);
			return respon;
		}
		
		
		
		@RequestMapping(value="getAll",method={RequestMethod.GET})
		public HashMap<String, Object> getUserAll(){
			HashMap<String, Object> respon = new  HashMap<String,Object>();
			respon.put("Response", service.findAll());
			respon.put("success", Boolean.TRUE);
			return respon;
		}
		
		
		

}
