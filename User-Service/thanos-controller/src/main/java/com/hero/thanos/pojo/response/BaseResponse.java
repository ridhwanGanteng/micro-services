package com.hero.thanos.pojo.response;

import java.util.Map;

public class BaseResponse {

	private String responseCode;
	private String responseDesc;
	private Map<String, Object> respon;
	
	public void setRespon(Map<String, Object> respon) {
		this.respon = respon;
	}
	
	public Map<String, Object> getRespon() {
		return respon;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}


}
