package com.hero.thanos.pojo.request;

public class ReqLogin {
	
	private String nama;
	private String password;
	private String role;
	
	public ReqLogin() {
		// TODO Auto-generated constructor stub
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	public String getRole() {
		return role;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNama() {
		return nama;
	}

}
