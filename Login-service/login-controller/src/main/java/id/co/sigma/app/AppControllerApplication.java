package id.co.sigma.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan
public class AppControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppControllerApplication.class, args);
	}
}
