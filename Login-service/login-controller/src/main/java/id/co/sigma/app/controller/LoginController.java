package id.co.sigma.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.app.feign.ThanosInterface;
import id.co.sigma.app.pojo.request.ReqLogin;
import id.co.sigma.app.pojo.response.BaseResponse;
import id.co.sigma.app.services.LoginService;

@RestController
@RequestMapping(value="rest/user")
public class LoginController {

	@Autowired
	private LoginService service;
	
	private Log log = LogFactory.getLog(this.getClass());
	
	private SimpleDateFormat sdf =new SimpleDateFormat("dd-mm-yy (hh:mm:ss)");
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public BaseResponse Login(@RequestBody ReqLogin obj){
		
		BaseResponse respon = new BaseResponse();
		log.info("##USER NAMA = "+obj.getNama());
		String user = service.login(obj.getNama(),obj.getPassword());
		respon.setResponseCode("Time : "+sdf.format(new Date()));
		if(user.equalsIgnoreCase("00")){
			respon.setResponseDesc(user);
		}
		return respon;
	}
	
	@RequestMapping(value="/test",method=RequestMethod.GET)
	public BaseResponse Login(){
		BaseResponse respon = new BaseResponse();
		respon.setResponseCode("00");
		respon.setResponseDesc("Oke connect. dari Layer pertama");
		return respon;
	}
	
	
}
