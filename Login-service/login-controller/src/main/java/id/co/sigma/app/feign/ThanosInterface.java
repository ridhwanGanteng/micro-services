package id.co.sigma.app.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.sigma.app.pojo.request.ReqLogin;

@FeignClient("thanos")
public interface ThanosInterface {
	@HystrixCommand
	@HystrixProperty(name="hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value="3000")
	@RequestMapping(value="/api/user/login",method=RequestMethod.POST)
	public String login(@RequestBody ReqLogin req);

}
