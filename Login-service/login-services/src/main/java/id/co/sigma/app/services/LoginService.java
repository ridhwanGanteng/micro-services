package id.co.sigma.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.app.dao.ThanosInterface;
import id.co.sigma.app.model.ReqLogin;

@Service
public class LoginService{

	@Autowired
	private ThanosInterface service;
	
	public String login(String nama, String password) {
		ReqLogin l = new ReqLogin();
		l.setNama(nama);
		l.setPassword(password);
		return service.login(l);
	}

}
