package id.co.sigma.app.model;

public class ReqLogin {
	private String nama;
	private String password;

	public ReqLogin() {
		// TODO Auto-generated constructor stub
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
