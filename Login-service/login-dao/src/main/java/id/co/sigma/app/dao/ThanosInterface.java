package id.co.sigma.app.dao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.sigma.app.model.ReqLogin;

@FeignClient("User-Service")
public interface ThanosInterface {
	@HystrixCommand
	@HystrixProperty(name="hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value="3000")
	@RequestMapping(value="/api/user/login",method=RequestMethod.POST)
	public String login(@RequestBody ReqLogin req);

}
